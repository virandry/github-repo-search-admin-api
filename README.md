**Objectives**

🥅 Submit your assignment as a Git repository hosted on any SCM.

🏌 Include a README explaining how to install dependencies and run your application.

💉 Include unit and/or integration tests and instructions for running them.

✂️ Explain any compromises/shortcuts your made due to time considerations.


# Github Repo Search Admin API (🏌)
## Demo Video
https://www.loom.com/share/389d24c596594a379c052a3c6a9bb777
## Repository Links (🥅)
1. Audit API: https://gitlab.com/virandry/github-repo-search-admin-api
2. Github Repo Search: https://gitlab.com/virandry/github-repo-search

## Pre-requisite
1. Java 8 or above
2. [Maven](https://maven.apache.org/install.html)
3. yarn/npm

## Tech Stack
### Front-end
* React
### Back-end
* Java
* Spring Boot 2
* H2

## Step-by-step
### Audits API
1. Clone the project
    ```shell script
    git clone git@gitlab.com:virandry/github-repo-search-admin-api.git grs-admin
    ```
2. Go to **grs-admin** directory
    ```shell script
    cd grs-admin
    ```
3. Download dependencies and build the project with Maven
    ```shell script
    mvn clean package
    ``` 
4. Run the package with java -jar:
    ```shell script
    java -jar target/grs-0.0.1-SNAPSHOT.jar 
    ``` 
   **OR** with maven
    ```shell script
    mvn spring-boot:run
    ``` 
5. Check if the api is working: an empty array in response body with Http status 200.
    ```shell script
    curl -u rovr:b8960fe6-c443-4568-a0f1-db2186887bde http://localhost:8080/reports/audits
    ```
   Expected response
   ```json
   {"content":[],"totalElements":0,"totalPages":0,"size":20,"page":0}
   ```

### Front-end
* `cd ..` if you were in grs-admin directory.
1. Clone the project
    ```shell script
    git clone git@gitlab.com:virandry/github-repo-search.git grs
    ```
2. Go to **grs** directory
    ```shell script
    cd grs
    ```
3. Install dependencies and build the project
    ```shell script
    yarn install
    yarn build
    ```
4. Run `yarn start:prod`
5. Open browser and go to **http://localhost:3000**

## Test (💉)
### Backend
_By default, `mvn clean package` will run the test before packaging._
1. Go to **grs-admin** directory
2. run test using maven cli
    ```shell script
    mvn clean test
    ```
   Expected Result:
    ```shell script
    [INFO] 
    [INFO] Results:
    [INFO] 
    [INFO] Tests run: 14, Failures: 0, Errors: 0, Skipped: 0
    [INFO] 
    ```
### Front-end
1. Go to **grs** directory
2. run test using maven cli
    ```shell script
    yarn install && yarn test
    ```
   Expected Result:
    ```shell script
    Test Suites: 5 passed, 5 total
    Tests:       12 passed, 12 total
    Snapshots:   0 total
    Time:        3.983s
    Ran all test suites.
    Done in 4.49s.
    ```

## How To
1. Open browser
2. Go to http://localhost:3000
3. Start searching for repositories by topic and/or language
4. Once finished performing several searches, let's take a look on admin page by clicking the Admin link in the top right of the page.
5. If you are not logged in, it will redirect you to /login page
6. by default, there is only one user stored in database when the API is initiated
    ```shell script
    username: rovr
    password: rovr123
    ```
7. Browse the admin page. enjoy!

## H2 Console
H2 is chosen for this demo as there is no setup required to run the backend.
User **rovr** is inserted when the the backend is started. It can be checked via h2 web console.
1. Go to http://localhost:8080/console
2. Login with credentials below
```
Driver Class: org.h2.Driver
JDBC URL: jdbc:h2:mem:grsdb
User Name: grsadmin
Password:: grsadmin123
```

## Basic Authentication for Reports API
Most of the api used are public but reports API
```
|:-------------------------------:|------------------------|
| Github API v3                   | public                 |
| http://localhost:8080/login     | public                 |
| http://localhost:8080/audits    | public                 |
| http://localhost:8080/reports/* | Basic auth is required |
|:-------------------------------:|------------------------| 
```

### Authentication Flow
```shell script
+------------------------------------+
|                                    |
| [POST] http://localhost:8080/login |
| [BODY] {                           |
|            username: "rovr",       |
|            password: "rovr123"     |
|        }                           |
|                                    |
+------------------+-----------------+
                   |
                   |
                   v
+------------------+-----------------------------+
|                                                |
| [GRS API]                                      |
| 1. Validate username & password                |
| 2. if valid, generate new token                |
| 3. return the token in response body as string |
|                                                |
+------------------+-----------------------------+
                   |
                   |
                   v
+------------------+-------------------+
|                                      |
| [RESPONSE BODY] <TOKEN>              |
| b8960fe6-c443-4568-a0f1-db2186887bde |
|                                      |
+--------------------------+-----------+
                           |
                           |
                           v
+-------------------------------------------------------+
|                                                       |
| In client side:                                       |
| 1. base64 encode with this format:                    |
|    <username>:<token>                                 |
| 2. js code                                            |
|    btoa("rovr:b8960fe6-c443-4568-a0f1-db2186887bde")  |
| 3. store to localstorage with key named: encodedBasic |
|                                                       |
+--------------------------+----------------------------+
                           |
                           |
                           v
+--------------------------+-----------------------------+
|                                                        |
| Upon calling these endpoints:                          |
| • http://localhost:8080/reports/audits                 |
| • http://localhost:8080/reports/top-languages          |
| • http://localhost:8080/reports/top-topics             |
| • http://localhost:8080/reports/searches/total         |
|                                                        |
| 1. Get the encoded token from local storage            |
| 2. Prepend the token with "Basic "                     |
| 3. add Authorization to Request Header with that value |
|                                                        |
+-------------------------+------------------------------+
                          |
                          |
                          v
+-------------------------+----------------------------+
|                                                      |
| Without providing valid Basic authorization header,  |
| the protected endpoints will return HTTP status 401. |
| Which make the GRS front-end to redirect             |
| from /admin to /login                                |
|                                                      |
+------------------------------------------------------+

```

## Compromises/shortcuts made due to time considerations. ✂
### Front-end
* /admin: Table header is not sticky to the top when it is scrolled down to load more data, there is not table header to explain the column.
* /admin: No sort functionality in the front-end apps while backend provide sorting capability
* Lack of test
* Using outdated library without porting over and rewritten it: parse-link-header
    * It is a best practice to port over and rewrite the outdated library as vanilla as possible
    * Keep the license in the ported code
* Using Pagination component by material-ui
* Exceeding Github Rate Limit error is not handled yet
* Fully optimize bundling such as code-spliting, dependency optimization, single module import enforcement, etc.
    * However, serving brotli & gzip compression are there.
### Back-end
* There is no test profile to execute integration test and backend test separately.

## ,etc.
### Rate Limiting
Github limit unauthenticated requests up to 60 requests per hour and 10 repetitive request in certain short period of time.

I provide a method to include [**Basic Auth to Github API**](https://developer.github.com/v3/auth/#via-oauth-and-personal-access-tokens) to increase the X-RATE-LIMIT.

To generate the personal access token, [click here](https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token-for-the-command-line)

```shell script
yarn build --env.USERNAME=<username> --env.TOKEN=<personal_access_token>
yarn start:prod
```

### Maximus Search Results
GitHub Search API provides up to 1,000 results for each search. 
Hence, the maximum page shown in GRS is 84 as we request 12 items per page. 

### Inspect Bundle
To inspect the front-end app bundling size: `yarn build --env.inspectBundle`