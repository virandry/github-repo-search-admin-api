package online.virandry.grs.repository;

import online.virandry.grs.model.Audit;
import online.virandry.grs.model.StatItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AuditRepository extends JpaRepository<Audit, Long> {

    @Query("Select a from Audit a")
    Page<Audit> findAll(Pageable pageable);

    @Query(nativeQuery = true, value = "SELECT language AS name, COUNT(language) AS COUNT FROM ( SELECT LOWER(language) AS language FROM audit WHERE action = 'SUBMIT' AND language != '' ) GROUP BY language ORDER BY count DESC LIMIT 5")
    List<StatItem> findTop5SearchByLanguage();

    @Query(nativeQuery = true, value = "SELECT topic AS name, COUNT(topic) AS COUNT FROM ( SELECT LOWER(topic) AS topic FROM audit WHERE action = 'SUBMIT' AND topic != '' ) GROUP BY topic ORDER BY count DESC LIMIT 5;")
    List<StatItem> findTop5SearchByTopic();

    @Query(nativeQuery = true, value = "SELECT Count(id) AS count FROM audit WHERE action = 'SUBMIT'")
    int getTotalNumberOfSearch();

}
