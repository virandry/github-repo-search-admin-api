package online.virandry.grs.dto;

import com.fasterxml.jackson.databind.JsonNode;
import online.virandry.grs.constant.SearchAction;
import online.virandry.grs.model.Audit;

import java.time.LocalDateTime;

public class AuditDto {
    private String topic;
    private String language;
    private String url;
    private int page;
    private int totalPage;
    private int totalCount;
    private int perPage;
    private JsonNode result;
    private SearchAction action;

    public AuditDto() {
    }

    public AuditDto(String topic, String language, String url, int page, int totalPage, int totalCount, int perPage, JsonNode result, SearchAction action) {
        this.topic = topic;
        this.language = language;
        this.url = url;
        this.page = page;
        this.totalPage = totalPage;
        this.perPage = perPage;
        this.result = result;
        this.action = action;
    }

    public AuditDto(Audit audit) {
        this.topic = audit.getTopic();
        this.language = audit.getLanguage();
        this.url = audit.getUrl();
        this.page = audit.getPage();
        this.totalPage = audit.getTotalPage();
        this.totalCount = audit.getTotalCount();
        this.perPage = audit.getPerPage();
        this.result = audit.getResult();
        this.action = audit.getAction();
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public JsonNode getResult() {
        return result;
    }

    public void setResult(JsonNode result) {
        this.result = result;
    }

    public SearchAction getAction() {
        return action;
    }

    public void setAction(SearchAction action) {
        this.action = action;
    }
}
