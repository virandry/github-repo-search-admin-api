package online.virandry.grs.dto;

import org.springframework.data.domain.Page;

import java.util.List;

public class PageDTO<T> {
    List<T> content;
    long totalElements;
    long totalPages;
    long size;
    long page;

    public PageDTO(List<T> list) {
        content = list;
    }

    public PageDTO(Page<T> page) {
        this.content = page.getContent();
        this.totalElements = page.getTotalElements();
        this.totalPages = page.getTotalPages();
        this.size = page.getSize();
        this.page = page.getNumber();
    }

    public PageDTO() {
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public List<T> getContent() {
        return this.content;
    }

    public long getTotalElements() {
        return this.totalElements;
    }

    public long getTotalPages() {
        return this.totalPages;
    }

    public long getSize() {
        return this.size;
    }

    public long getPage() {
        return this.page;
    }
}
