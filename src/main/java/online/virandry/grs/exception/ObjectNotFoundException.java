package online.virandry.grs.exception;

public class ObjectNotFoundException extends Exception {
    Long auditId;
    ExceptionCodes exceptionCode = ExceptionCodes.OBJECT_NOT_FOUND;

    public ObjectNotFoundException(String message, Long auditId) {
        super(message);
        this.auditId = auditId;
    }

    public ObjectNotFoundException(String message) {
        super(message);
    }

}

