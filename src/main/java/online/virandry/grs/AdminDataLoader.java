package online.virandry.grs;

import online.virandry.grs.model.User;
import online.virandry.grs.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Component
public class AdminDataLoader {

    @Autowired
    UserRepository userRepository;

    @PostConstruct
    public void loadData() {
        User user = new User();
        user.setUsername("rovr");
        user.setPassword("rovr123");
        user.setToken(UUID.randomUUID());
        userRepository.save(user);
    }

}