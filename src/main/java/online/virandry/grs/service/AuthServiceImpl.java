package online.virandry.grs.service;

import online.virandry.grs.model.User;
import online.virandry.grs.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    UserRepository userRepository;

    @Override
    public boolean validate(User user) {
        User found = userRepository.findByUsername(user.getUsername());
        return user.getToken().equals(found.getToken());
    }

    @Override
    public UUID login(String username, String password) {
        User user = userRepository.findByUsername(username);
        // no hashing for this demo auth
        if (password.equals(user.getPassword())) {
            UUID generatedToken = UUID.randomUUID();
            user.setToken(generatedToken);
            userRepository.save(user);
            return generatedToken;
        }
        return null;
    }
}
