package online.virandry.grs.service;

import online.virandry.grs.dto.AuditDto;
import online.virandry.grs.model.Audit;
import online.virandry.grs.model.StatItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AuditService {

    public void logAudit(AuditDto auditDto);

    public List<Audit> getAudits();

    public Page<Audit> getAudits(Pageable pageable);

    public List<StatItem> findTop5SearchByLanguage();

    public List<StatItem> findTop5SearchByTopic();

    public int getTotalNumberOfSearch();

}
