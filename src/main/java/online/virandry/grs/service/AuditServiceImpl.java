package online.virandry.grs.service;

import online.virandry.grs.dto.AuditDto;
import online.virandry.grs.model.Audit;
import online.virandry.grs.model.StatItem;
import online.virandry.grs.repository.AuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AuditServiceImpl implements AuditService {
    @Autowired
    AuditRepository auditRepository;

    public void logAudit(AuditDto auditDto) {
        auditRepository.save(toEntity(auditDto));
    }

    public List<Audit> getAudits() {
        return (List<Audit>) auditRepository.findAll();
    }

    @Override
    public Page<Audit> getAudits(Pageable pageable) {
        return auditRepository.findAll(pageable);
    }

    public List<StatItem> findTop5SearchByLanguage() {
        return auditRepository.findTop5SearchByLanguage();
    }

    public List<StatItem> findTop5SearchByTopic() {
        return auditRepository.findTop5SearchByTopic();
    }

    public int getTotalNumberOfSearch() {
        return auditRepository.getTotalNumberOfSearch();
    }

    private Audit toEntity(AuditDto auditDto) {
        Audit audit = new Audit();
        audit.setTopic(auditDto.getTopic());
        audit.setLanguage(auditDto.getLanguage());
        audit.setPage(auditDto.getPage());
        audit.setPerPage(auditDto.getPerPage());
        audit.setUrl(auditDto.getUrl());
        audit.setResult(auditDto.getResult());
        audit.setTotalPage(auditDto.getTotalPage());
        audit.setTotalCount(auditDto.getTotalCount());
        audit.setTime(LocalDateTime.now());
        audit.setAction(auditDto.getAction());
        return audit;
    }

}
