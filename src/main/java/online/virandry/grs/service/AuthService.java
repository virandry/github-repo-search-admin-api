package online.virandry.grs.service;

import online.virandry.grs.model.User;

import java.util.UUID;

public interface AuthService {
    public boolean validate(User user);
    public UUID login(String username, String password);
}
