package online.virandry.grs.model;

public interface StatItem {
    String getName();
    int getCount();
}
