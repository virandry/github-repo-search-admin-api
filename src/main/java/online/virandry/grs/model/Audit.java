package online.virandry.grs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import online.virandry.grs.constant.SearchAction;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class),
})
@Entity
@Table(name = "audit")
public class Audit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String topic;

    @Column
    private String language;

    @Column
    private LocalDateTime time;

    @Column
    private String url;

    @Column
    private int page;

    @Column
    private int totalPage;

    @Column
    private int totalCount;

    @Column
    private int perPage;

    @Enumerated(EnumType.STRING)
    @Column
    private SearchAction action;


    @Type(type = "json")
    @Column(columnDefinition = "json")
    private JsonNode result;

    public Audit() {

    }

    public Audit(String topic, String language, LocalDateTime time, String url, int page, int totalPage, int totalCount, int perPage, JsonNode result, SearchAction action) {
        this.topic = topic;
        this.language = language;
        this.time = time;
        this.url = url;
        this.page = page;
        this.totalPage = totalPage;
        this.totalCount = totalCount;
        this.perPage = perPage;
        this.result = result;
        this.action = action;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public JsonNode getResult() {
        return result;
    }

    public void setResult(JsonNode result) {
        this.result = result;
    }

    public SearchAction getAction() {
        return action;
    }

    public void setAction(SearchAction action) {
        this.action = action;
    }
}
