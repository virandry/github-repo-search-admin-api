package online.virandry.grs.controller;

import online.virandry.grs.dto.AuthDTO;
import online.virandry.grs.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/login")
public class AuthController extends AbstractController {

    @Autowired
    private AuthService authService;

    @PostMapping()
    public ResponseEntity<?> login(@RequestBody AuthDTO authDTO) {
        UUID token = authService.login(authDTO.getUsername(), authDTO.getPassword());
        return ResponseEntity.ok(token.toString());
    }
}
