package online.virandry.grs.controller;

import online.virandry.grs.dto.AuditDto;
import online.virandry.grs.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/audits")
public class AuditController extends AbstractController {

    @Autowired
    private AuditService auditService;

    @PostMapping()
    public ResponseEntity<?> logAudit(@RequestBody AuditDto auditDto) {
        auditService.logAudit(auditDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
