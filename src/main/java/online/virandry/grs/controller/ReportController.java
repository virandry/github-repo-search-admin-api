package online.virandry.grs.controller;

import online.virandry.grs.dto.PageDTO;
import online.virandry.grs.model.Audit;
import online.virandry.grs.model.StatItem;
import online.virandry.grs.model.User;
import online.virandry.grs.service.AuditService;
import online.virandry.grs.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/reports")
public class ReportController extends AbstractController {

    @Autowired
    private AuditService auditService;

    @Autowired
    private AuthService authService;

    @GetMapping(path = "/top-languages", produces = "application/json")
    public ResponseEntity<List<StatItem>> findTop5SearchByLanguage(@RequestHeader("Authorization") String auth) {
        if (!authService.validate(parseAuthorization(auth))) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        List<StatItem> stats = auditService.findTop5SearchByLanguage();
        return ResponseEntity.ok(stats);
    }

    @GetMapping(path = "/top-topics", produces = "application/json")
    public ResponseEntity<List<StatItem>> findTop5SearchByTopic(@RequestHeader("Authorization") String auth) {
        if (!authService.validate(parseAuthorization(auth))) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        List<StatItem> stats = auditService.findTop5SearchByTopic();
        return ResponseEntity.ok(stats);
    }

    @GetMapping(path = "/searches/total", produces = "application/json")
    public ResponseEntity<?> getTotalNumberOfSearch(@RequestHeader("Authorization") String auth) {
        if (!authService.validate(parseAuthorization(auth))) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        int numberOfSearch = auditService.getTotalNumberOfSearch();
        return ResponseEntity.ok(numberOfSearch);
    }

    @GetMapping(path = "/audits", produces = "application/json")
    public ResponseEntity<PageDTO<Audit>> findAll(Pageable pageable, @RequestHeader("Authorization") String auth) {
        if (!authService.validate(parseAuthorization(auth))) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        Page<Audit> audits = auditService.getAudits(pageable);
        logInfo(audits);
        return ResponseEntity.ok(new PageDTO<>(audits));
    }

}
