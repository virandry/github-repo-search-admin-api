package online.virandry.grs.controller;

import online.virandry.grs.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

@CrossOrigin(origins = {"${cors.fe}"})
public abstract class AbstractController {
    private static final Logger logger = LoggerFactory.getLogger(AbstractController.class);

    public static void logInfo(Object o) {
        logger.info("\r\n\r\n INFO: " + o + "\r\n\r\n");
    }

    public static void logDebug(Object o) {
        logger.debug("\r\n\r\n INFO: " + o + "\r\n\r\n");
    }

    public static void logError(Object o) {
        logger.error("\r\n\r\n INFO: " + o + "\r\n\r\n");
    }

    public User parseAuthorization(String auth) {
        User user = new User();
        if (auth != null && auth.toLowerCase().startsWith("basic")) {
            // Authorization: Basic base64credentials
            String base64Credentials = auth.substring("Basic".length()).trim();
            byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
            String credentials = new String(credDecoded, StandardCharsets.UTF_8);
            // credentials = username:password
            final String[] values = credentials.split(":", 2);
            String username = values[0];
            String token = values[1];
            logInfo(username);
            logInfo(token);
            user.setUsername(username);
            user.setToken(UUID.fromString(token)); // TODO: not safe
        }
        return user;
    }
}