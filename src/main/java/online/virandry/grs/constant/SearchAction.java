package online.virandry.grs.constant;

public enum SearchAction {
    SUBMIT, PAGE_CHANGE
}
