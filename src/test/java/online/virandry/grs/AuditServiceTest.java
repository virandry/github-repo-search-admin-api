package online.virandry.grs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import online.virandry.grs.constant.SearchAction;
import online.virandry.grs.model.Audit;
import online.virandry.grs.model.StatItem;
import online.virandry.grs.repository.AuditRepository;
import online.virandry.grs.service.AuditService;
import online.virandry.grs.service.AuditServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class AuditServiceTest {

    @Autowired
    private AuditService auditService;
    @MockBean
    private AuditRepository auditRepository;
    private ObjectMapper objectMapper = new ObjectMapper();
    private Audit audit = createTestAudit();
    private StatItem statItem;
    private int numberOfSearch = 2000;

    @Before
    public void setUp() {
        List<Audit> audits = new ArrayList<>();
        audits.add(audit);

        Mockito.when(auditRepository.findAll())
                .thenReturn(audits);

        statItem = new StatItem() {
            @Override
            public String getName() {
                return "TypeScript";
            }

            @Override
            public int getCount() {
                return 12;
            }
        };
        List<StatItem> statItems = new ArrayList<>();
        statItems.add(statItem);

        Mockito.when(auditRepository.findTop5SearchByLanguage()).thenReturn(statItems);
        Mockito.when(auditRepository.findTop5SearchByTopic()).thenReturn(statItems);
        Mockito.when(auditRepository.getTotalNumberOfSearch()).thenReturn(numberOfSearch);
    }

    @Test
    public void whenFindAll_thenAuditsShouldBeFound() {
        List<Audit> audits = auditService.getAudits();

        assertThat(audits.get(0).getTopic())
                .isEqualTo(audit.getTopic());

        assertThat(audits.get(0).getLanguage())
                .isEqualTo(audit.getLanguage());
    }

    @Test
    public void whenFindTop5SearchByLanguage_thenStatItemListShouldBeFound() {
        List<StatItem> statItems = auditService.findTop5SearchByLanguage();

        assertThat(statItems.get(0).getName())
                .isEqualTo(statItem.getName());
        assertThat(statItems.get(0).getCount())
                .isEqualTo(statItem.getCount());

    }

    @Test
    public void whenFindTop5SearchByTopic_thenStatItemListShouldBeFound() {
        List<StatItem> statItems = auditService.findTop5SearchByTopic();

        assertThat(statItems.get(0).getName())
                .isEqualTo(statItem.getName());
        assertThat(statItems.get(0).getCount())
                .isEqualTo(statItem.getCount());

    }

    @Test
    public void whenGetTotalNumberOfSearch_thenIntegerValueShouldBeFound() {
        int found = auditService.getTotalNumberOfSearch();

        assertThat(found)
                .isEqualTo(numberOfSearch);

    }

    private Audit createTestAudit() {
        JsonNode result = null;
        try {
            result = objectMapper.readTree(TestData.SEARCH_RESULT);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Audit audit = new Audit();
        audit.setTopic("Tetris");
        audit.setLanguage("TypeScript");
        audit.setTime(LocalDateTime.now());
        audit.setUrl("https://api.github.com/search/repositories?q=topic:tetris+language:TypeScript&page=1&per_page=10");
        audit.setPerPage(10);
        audit.setPage(1);
        audit.setTotalPage(100);
        audit.setTotalCount(1000);
        audit.setResult(result);
        audit.setAction(SearchAction.SUBMIT);
        return audit;
    }

    @TestConfiguration
    static class AuditServiceTestContextConfiguration {
        @Bean
        public AuditService auditService() {
            return new AuditServiceImpl();
        }
    }
}
