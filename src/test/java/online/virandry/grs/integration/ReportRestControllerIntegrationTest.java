package online.virandry.grs.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import online.virandry.grs.GrsApplication;
import online.virandry.grs.TestData;
import online.virandry.grs.constant.SearchAction;
import online.virandry.grs.dto.AuditDto;
import online.virandry.grs.model.Audit;
import online.virandry.grs.model.User;
import online.virandry.grs.repository.AuditRepository;
import online.virandry.grs.repository.UserRepository;
import online.virandry.grs.service.AuditService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GrsApplication.class})
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
public class ReportRestControllerIntegrationTest extends AbstractTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuditRepository auditRepository;

    @Autowired
    private AuditService auditService;

    private ObjectMapper objectMapper = new ObjectMapper();

    private String basicToken = "";

    @Before
    public void setUp() {
        User user = userRepository.findByUsername("rovr");
        String encoded = Base64.getEncoder().encodeToString((user.getUsername() + ":" + user.getToken()).getBytes());
        this.basicToken = "Basic " + encoded;
    }

    @Test
    public void noAudits_whenGetAudits_thenStatus200AndContentIsEmptyArray()
            throws Exception {

        MvcResult mvcResult = mvc.perform(get("/reports/audits")
                .header("Authorization", basicToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).andReturn();

        ObjectMapper objectMapper = new ObjectMapper();
        assertThat(objectMapper.readTree(mvcResult.getResponse().getContentAsString()).get("content").isEmpty()).isTrue();
        String authorization = mvcResult.getRequest().getHeader("Authorization");

    }

    @Test
    public void givenAudits_whenGetAudits_thenStatus200AndContentIsNotEmptyArray()
            throws Exception {
        Audit audit = createTestAudit();
        auditService.logAudit(new AuditDto(audit));

        MvcResult mvcResult = mvc.perform(get("/reports/audits")
                .header("Authorization", basicToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).andReturn();

        assertThat(objectMapper.readTree(mvcResult.getResponse().getContentAsString()).get("content").isEmpty()).isFalse();

    }


    @Test
    public void givenAudits_whenGetTopLanguages_thenStatus200AndContentIsNotEmptyArray()
            throws Exception {
        Audit audit = createTestAudit();
        auditService.logAudit(new AuditDto(audit));

        MvcResult mvcResult = mvc.perform(get("/reports/top-languages")
                .header("Authorization", basicToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).andReturn();

        assertThat(objectMapper.readTree(mvcResult.getResponse().getContentAsString()).get(0).get("name").asText()).isEqualTo(audit.getLanguage().toLowerCase());
        assertThat(objectMapper.readTree(mvcResult.getResponse().getContentAsString()).get(0).get("count").asText()).isEqualTo("1");

    }


    @Test
    public void givenAudits_whenGetTopTopics_thenStatus200AndContentIsNotEmptyArray()
            throws Exception {
        Audit audit = createTestAudit();
        auditService.logAudit(new AuditDto(audit));

        MvcResult mvcResult = mvc.perform(get("/reports/top-topics")
                .header("Authorization", basicToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).andReturn();

        assertThat(objectMapper.readTree(mvcResult.getResponse().getContentAsString()).get(0).get("name").asText()).isEqualTo(audit.getTopic().toLowerCase());
        assertThat(objectMapper.readTree(mvcResult.getResponse().getContentAsString()).get(0).get("count").asText()).isEqualTo("1");

    }

    private Audit createTestAudit() {
        JsonNode result = null;
        try {
            result = objectMapper.readTree(TestData.SEARCH_RESULT);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Audit audit = new Audit();
        audit.setTopic("Tetris");
        audit.setLanguage("TypeScript");
        audit.setTime(LocalDateTime.now());
        audit.setUrl( "https://api.github.com/search/repositories?q=topic:tetris+language:TypeScript&page=1&per_page=10");
        audit.setPerPage(10);
        audit.setPage(1);
        audit.setTotalPage(100);
        audit.setTotalCount(1000);
        audit.setResult(result);
        audit.setAction(SearchAction.SUBMIT);
        return audit;
    }

    private String createTestAuditAsString() throws JsonProcessingException {
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        return objectWriter.writeValueAsString(createTestAudit());
    }
}
