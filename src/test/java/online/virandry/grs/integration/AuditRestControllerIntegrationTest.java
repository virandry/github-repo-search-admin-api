package online.virandry.grs.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import online.virandry.grs.GrsApplication;
import online.virandry.grs.TestData;
import online.virandry.grs.constant.SearchAction;
import online.virandry.grs.model.Audit;
import online.virandry.grs.repository.AuditRepository;
import online.virandry.grs.service.AuditService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GrsApplication.class})
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
public class AuditRestControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private AuditRepository auditRepository;

    @Autowired
    private AuditService auditService;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void givenAudits_whenPostAudit_thenStatus201()
            throws Exception {

        List<Audit> before = auditService.getAudits();
        assertThat(before).isEmpty();

        String audit = createTestAuditAsString();

        mvc.perform(post("/audits")
                .contentType(MediaType.APPLICATION_JSON).content(audit))
                .andExpect(status().isCreated());

        List<Audit> after = auditService.getAudits();
        assertThat(after).isNotEmpty();
    }


    private Audit createTestAudit() {
        JsonNode result = null;
        try {
            result = objectMapper.readTree(TestData.SEARCH_RESULT);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Audit audit = new Audit();
        audit.setTopic("Tetris");
        audit.setLanguage("TypeScript");
        audit.setTime(LocalDateTime.now());
        audit.setUrl( "https://api.github.com/search/repositories?q=topic:tetris+language:TypeScript&page=1&per_page=10");
        audit.setPerPage(10);
        audit.setPage(1);
        audit.setTotalPage(100);
        audit.setTotalCount(1000);
        audit.setResult(result);
        audit.setAction(SearchAction.SUBMIT);
        return audit;
    }

    private String createTestAuditAsString() throws JsonProcessingException {
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        return objectWriter.writeValueAsString(createTestAudit());
    }
}
