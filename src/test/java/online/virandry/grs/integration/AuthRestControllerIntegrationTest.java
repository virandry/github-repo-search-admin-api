package online.virandry.grs.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import online.virandry.grs.GrsApplication;
import online.virandry.grs.TestData;
import online.virandry.grs.constant.SearchAction;
import online.virandry.grs.dto.AuthDTO;
import online.virandry.grs.model.Audit;
import online.virandry.grs.model.User;
import online.virandry.grs.repository.UserRepository;
import online.virandry.grs.service.AuthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GrsApplication.class})
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
public class AuthRestControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthService authService;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void givenCredential_whenPostLogin_thenReturnToken()
            throws Exception {


        AuthDTO authDTO = new AuthDTO("rovr", "rovr123");
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();

        MvcResult mvcResult = mvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON).content(objectWriter.writeValueAsString(authDTO)))
                .andExpect(status().isOk()).andReturn();

        assertThat(isValidUUID(mvcResult.getResponse().getContentAsString())).isTrue();
    }

    private boolean isValidUUID(String uuid) {
        try{
            UUID.fromString(uuid);
            return true;
        } catch (IllegalArgumentException exception){
            return false;
        }
    }

}
