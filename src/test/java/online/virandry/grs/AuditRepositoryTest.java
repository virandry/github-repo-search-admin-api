package online.virandry.grs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import online.virandry.grs.TestData;
import online.virandry.grs.constant.SearchAction;
import online.virandry.grs.dto.AuditDto;
import online.virandry.grs.model.Audit;
import online.virandry.grs.model.StatItem;
import online.virandry.grs.repository.AuditRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AuditRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AuditRepository auditRepository;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void whenFindAll_thenReturnAudits() {
        Audit audit = createTestAudit();
        entityManager.persist(audit);
        entityManager.flush();

        List<Audit> found = auditRepository.findAll();

        assertThat(found.get(0).getTopic())
                .isEqualTo(audit.getTopic());

        assertThat(found.get(0).getLanguage())
                .isEqualTo(audit.getLanguage());

        assertThat(found.get(0).getResult().get("id")).isEqualTo(audit.getResult().get("id"));

    }

    @Test
    public void whenLogAudit_thenStoreAudits() {
        Audit audit = createTestAudit();
        Audit stored = auditRepository.save(audit);

        assertThat(stored.getId()).isNotNull();

    }

    @Test
    public void whenFindTop5SearchByLanguage_thenReturStatItemList() {
        Audit audit = createTestAudit();
        entityManager.persist(audit);
        entityManager.flush();

        List<StatItem> statItem = auditRepository.findTop5SearchByLanguage();
        assertThat(statItem.size()).isNotZero();
    }

    @Test
    public void whenFindTop5SearchByTopic_thenReturStatItemList() {
        Audit audit = createTestAudit();
        entityManager.persist(audit);
        entityManager.flush();

        List<StatItem> statItem = auditRepository.findTop5SearchByTopic();
        assertThat(statItem.size()).isNotZero();
    }

    private Audit createTestAudit() {
        JsonNode result = null;
        try {
            result = objectMapper.readTree(TestData.SEARCH_RESULT);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Audit audit = new Audit();
        audit.setTopic("Tetris");
        audit.setLanguage("TypeScript");
        audit.setTime(LocalDateTime.now());
        audit.setUrl( "https://api.github.com/search/repositories?q=topic:tetris+language:TypeScript&page=1&per_page=10");
        audit.setPerPage(10);
        audit.setPage(1);
        audit.setTotalPage(100);
        audit.setTotalCount(1000);
        audit.setResult(result);
        audit.setAction(SearchAction.SUBMIT);
        return audit;
    }
}
